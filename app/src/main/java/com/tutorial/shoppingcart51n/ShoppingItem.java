package com.tutorial.shoppingcart51n;

import java.io.Serializable;

class ShoppingItem implements Serializable {
    public String itemName;
    public int itemPrice;

    public ShoppingItem(String itemName, int itemPrice) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }
}
