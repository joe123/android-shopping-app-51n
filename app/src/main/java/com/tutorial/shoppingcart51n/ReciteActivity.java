package com.tutorial.shoppingcart51n;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReciteActivity extends AppCompatActivity {
    TextView textView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipt);
        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);
        ShoppingItemWrapper shoppingItemWrapper = (ShoppingItemWrapper)
                getIntent().getSerializableExtra(MainActivity.DATA_KEY);
        List<ShoppingItem> shoppingItems = shoppingItemWrapper.shoppingItems;
        Map<ShoppingItem, Integer> shoppingMap = new HashMap<>();
        for (int i = 0; i < shoppingItems.size(); i++) {
            ShoppingItem item = shoppingItems.get(i);
            Integer number = shoppingMap.get(item);
            if (number == null)
                number = 1;
            else
                number++;
            shoppingMap.put(item, number);
        }
        String recite = "";
        int totalPrice = 0;
        for (Map.Entry<ShoppingItem, Integer> entry : shoppingMap.entrySet()) {
            ShoppingItem item = entry.getKey();
            int quantity = entry.getValue();
            recite += item.itemName + "          QTY: " + quantity +
                    "          Price: " + (quantity * item.itemPrice) + '\n';
            totalPrice += item.itemPrice * quantity;
        }
        recite += "Total price: " + totalPrice;
        textView.setText(recite);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ReciteActivity.this, "Payment done", Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}