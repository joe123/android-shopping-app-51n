package com.tutorial.shoppingcart51n;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ImageButton imageButton;
    RecyclerView rv;
    Spinner spinner;
    FloatingActionButton floatingActionButton;
    List<ShoppingItem> spinnerItems;
    List<ShoppingItem> shoppingItems;
    RecyclerAdapter adapter;
    public static final String DATA_KEY = "DATA_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageButton = findViewById(R.id.ib);
        rv = findViewById(R.id.rv);
        spinner = findViewById(R.id.spinner);
        floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ReciteActivity.class);
                ShoppingItemWrapper shoppingItemWrapper = new ShoppingItemWrapper();
                shoppingItemWrapper.shoppingItems = MainActivity.this.shoppingItems;
                intent.putExtra(DATA_KEY, shoppingItemWrapper);
                startActivityForResult(intent, 1);
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShoppingItem item = (ShoppingItem) spinner.getSelectedItem();
                shoppingItems.add(item);
                adapter.notifyDataSetChanged();
            }
        });
        setupSpinner();
        setupRecycler();
    }

    private void setupRecycler() {
        shoppingItems = new ArrayList<>();
        adapter = new RecyclerAdapter(shoppingItems);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
    }

    private void setupSpinner() {
        spinnerItems = new ArrayList<>();
        ShoppingItem item1 = new ShoppingItem("Apple", 10);
        ShoppingItem item2 = new ShoppingItem("Meat", 100);
        ShoppingItem item3 = new ShoppingItem("Fish", 50);
        ShoppingItem item4 = new ShoppingItem("Rice", 15);
        spinnerItems.add(item1);
        spinnerItems.add(item2);
        spinnerItems.add(item3);
        spinnerItems.add(item4);
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this,
                R.layout.spinner_item,
                spinnerItems);
        spinner.setAdapter(spinnerAdapter);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        shoppingItems.clear();
        adapter.notifyDataSetChanged();
    }
}