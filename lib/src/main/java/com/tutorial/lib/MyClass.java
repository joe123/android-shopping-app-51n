package com.tutorial.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MyClass {
    public static void main(String[] args) {
        int x = 10;
        int y = 10;
        System.out.println(x == y);
        Animal animal = new Animal();
        animal.speed = 10;
        Animal animal1 = new Animal();
        animal1.speed = 10;
        System.out.println(animal.equals(animal1));
    }
}