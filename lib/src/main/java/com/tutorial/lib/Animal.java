package com.tutorial.lib;

import java.util.Objects;

class Animal {
    int speed;

    @Override
    public boolean equals(Object o) {
        Animal animal = (Animal) o;
        return speed == animal.speed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(speed);
    }
}
